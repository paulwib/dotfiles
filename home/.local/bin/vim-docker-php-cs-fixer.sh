#!/usr/bin/env sh

# It seems wrong to mount /var/folders, but because theis fixer operates on a temporary file
# and doesn't support stdin, I'm not sure how else it would be possible?
# ALE docs mention mounting temp directory and using a vim function to find it:
#  :echo fnamemodify(tempname(), ':h:h')
# But how can I get that into this script?
docker run --rm -i -v ${VIMTMPDIR}:${VIMTMPDIR} ghcr.io/php-cs-fixer/php-cs-fixer:${FIXER_VERSION:-3-php8.3} "$@"
