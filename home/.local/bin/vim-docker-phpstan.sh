#!/usr/bin/env bash

# Workaround bug in ALE where it uses `--errorFormat` flag (camelCased),
# but should be `--error-format` (kebab-cased)
args=$(echo "$@" | sed 's/errorFormat/error-format/')
pwd=$(pwd)
docker run --rm -v "$pwd":"$pwd" ghcr.io/phpstan/phpstan $args
