# Follow XDG Base Directory Specification
# See https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_DATA_HOME="$HOME/.local"
export XDG_CACHE_HOME="$HOME/.cache"
if [ ! -w ${XDG_RUNTIME_DIR:="/run/user/$UID"} ]; then
  XDG_RUNTIME_DIR=/tmp
fi
export XDG_RUNTIME_DIR

# Base16 shell theme
# Override without editing file with `BASE16_THEME=atelier-cave-light source ~/.bash_profile`
# Note vim theme also uses base16, see vim/plugin/color/settings.vim
if [ -z "${BASE16_THEME}" ]; then
  export BASE16_THEME=atelier-cave
fi

# Additional sourced files
source "$XDG_CONFIG_HOME"/bash/exports
source "$XDG_CONFIG_HOME"/bash/aliases
source "$XDG_CONFIG_HOME"/bash/functions
source "$XDG_CONFIG_HOME"/bash/prompt

# Enable bash completion in interactive shells
# (note for OSX completion is also configured, see bash/osx/homebrew)
if ! shopt -oq posix; then
  if [ -f /usr/share/bash-completion/bash_completion  ]; then
    . /usr/share/bash-completion/bash_completion
  elif [ -f /etc/bash_completion  ]; then
    . /etc/bash_completion
  fi
fi

# Linux specific
if [[ "$OSTYPE" =~ ^linux ]]; then
  for f in "$XDG_CONFIG_HOME"/bash/linux/*; do
    source "$f"
  done
fi

# OSX specific
if [[ $OSTYPE == "darwin"* ]]; then
  for f in "$XDG_CONFIG_HOME"/bash/osx/*; do
    source "$f"
  done
fi

# Host specific - do not put these in version control!
if [ -f "$XDG_DATA_HOME"/host/"$HOSTNAME" ]; then
  source "$XDG_DATA_HOME"/host/"$HOSTNAME"
fi

# App specific
for f in "$XDG_CONFIG_HOME"/bash/apps/*; do
  source "$f"
done

# Case-insensitive globbing (used in pathname expansion)
shopt -s nocaseglob

# Append to the Bash history file, rather than overwriting it
shopt -s histappend

# When recalling a command from history with number like !123,
# put it on command line without executing
shopt -s histverify

# Autocorrect typos in path names when using `cd`
shopt -s cdspell

# Check the window size after each command and, if necessary,
# update the values of LINES and COLUMNS.
shopt -s checkwinsize