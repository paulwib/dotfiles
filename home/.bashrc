# .bashrc
#
# The Unix convention is `.bash_profile` is read at login and `.bashrc` is
# read for each new shell. OSX does [not respect the convention][1], it
# always reads from # `.bash_profile`.
#
# So this file will load `.bash_profile` if it has *not* already been loaded,
# to be more cross-platform compatible.
#
# [1]:https://scriptingosx.com/2017/04/about-bash_profile-and-bashrc-on-macos/

[ -n "$PS1" ] && source "$HOME/.bash_profile"