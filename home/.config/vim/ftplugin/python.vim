let b:ale_python_auto_poetry = 1
let b:ale_linters = ['ruff']
let b:ale_fixers = ['trim_whitespace', 'ruff_format']
