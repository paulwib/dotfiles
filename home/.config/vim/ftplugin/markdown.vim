let b:coc_suggest_disable = 1
let b:ale_linters = ['proselint']
let b:ale_fixers = ['trim_whitespace', 'remove_trailing_lines', 'prettier']
