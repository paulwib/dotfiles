let b:ale_linters = ['cargo', 'rls']
let b:ale_fixers = ['trim_whitespace', 'remove_trailing_lines', 'rustfmt']
let b:ale_rust_cargo_use_clippy = 1
