let b:ale_javascript_prettier_use_local_config = 1
let b:ale_linters = ['eslint']
let b:ale_fixers = ['trim_whitespace', 'prettier']
