" Using docker for PHP lint/fix as too much pain to set up a full PHP environment on a Mac

" Lint files with phpstan, running in Docker
let b:ale_linters = ['phpstan']
let b:ale_php_phpstan_executable = 'vim-docker-phpstan.sh'
let b:ale_php_phpstan_level = '4'

" Fix files with PHP CS Fixer, running in Docker
" Set the temp directory so shell script can mount it in docker as php_cs_fixer does not work with stdin
let $VIMTMPDIR = fnamemodify(tempname(), ':h:h')
let b:ale_fixers = ['php_cs_fixer']
let b:ale_php_cs_fixer_executable = 'vim-docker-php-cs-fixer.sh'
let b:ale_php_cs_fixer_options = '--rules=@Symfony,-braces_position'
