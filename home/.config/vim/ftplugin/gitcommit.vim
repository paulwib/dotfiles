" Use the same theme as the shell, as these files are regularly opened and closed
colorscheme base16-$BASE16_THEME

setlocal spell
