let g:NERDCreateDefaultMappings = 0

" ,/ to invert comment on the current line/selection
nmap <leader>/ :call nerdcommenter#Comment(0, "invert")<cr>
vmap <leader>/ :call nerdcommenter#Comment(0, "invert")<cr>

