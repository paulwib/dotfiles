" Airline settings - theming applied in colors.vim
let g:airline_powerline_fonts = 0
let g:airline#extensions#tabline#enabled = 1
let g:airline_section_z = '%3.3l/%-3.3L %2.3c'
