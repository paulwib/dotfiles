" Global options for ALE (Asynchronous Lint Engine)

" Displaying Language Server Protocol in ALE as can get that from CoC
" Also see :CocConfig which will send it's messages through ALE
" See https://github.com/dense-analysis/ale/blob/82c8e3a/README.md#5iii-how-can-i-use-ale-and-cocnvim-together
let g:ale_disable_lsp = 1

" Only run explictly configured linters
" The actual linters to be run should be specified in filetype plug-ins
" e.g. ftplugin/javascript.vim
" It's recommended to NOT install linters globally, instead rely on
" ALE to detect if they are installed for the current project.
let g:ale_linters_explicit = 1

" Always run any configured fixers on save
let g:ale_fix_on_save = 1

" Uncomment this to see output of calling external program with :AleInfo
"let g:ale_history_log_output = 1
