"
" Fuzzy Finder settings
"

" Function to include hidden files, except .git, in Ag results.
" The regex on the end is important to intialise the output window
" with all content which can then be filtered by typing more stuff.
autocmd! VimEnter * command! -nargs=* -bang AgHidden :call fzf#vim#ag_raw('--hidden --ignore=.git "^(?=.)"')

nmap ; :Buffers<CR>
nmap <Leader>f :Files<CR>
nmap <Leader>t :Tags<CR>
nmap <Leader>a :AgHidden<CR>
" Because I used to use Ctrlp
nmap <C-p> :Files<CR>

" Insert mode fuzzy find file name to insert
imap <C-x><C-f> <plug>(fzf-complete-path)
