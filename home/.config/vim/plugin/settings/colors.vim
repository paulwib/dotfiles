" Set path to base16-shell scripts so can use a different theme from terminal
let g:base16_shell_path='~/.local/base16-shell/scripts'
colorscheme base16-atelier-cave-light

" Make sure fzf matches the current colorscheme
let g:fzf_colors =
            \ { 'fg':      ['fg', 'Normal'],
            \ 'bg':      ['bg', 'Normal'],
            \ 'hl':      ['fg', 'Comment'],
            \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
            \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
            \ 'hl+':     ['fg', 'Statement'],
            \ 'info':    ['fg', 'PreProc'],
            \ 'border':  ['fg', 'Keyword'],
            \ 'prompt':  ['fg', 'Conditional'],
            \ 'pointer': ['fg', 'Exception'],
            \ 'marker':  ['fg', 'Keyword'],
            \ 'spinner': ['fg', 'Label'],
            \ 'header':  ['fg', 'Comment'] }

" This setting *should* make airline match base16-vim scheme
let g:airline_theme='base16_vim'
let g:airline_base16_improved_contrast = 1
let g:airline_base16_monotone = 1
