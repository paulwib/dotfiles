
" This allows sending a query directly to the current database
"
" First set the database with:
"   :DB g:db = <path to file or database connection>
"
" Then in normal mode use:
"    <C-Q><movement>
"
" The movement needs to be started from the beginning of the query.
" So to run a query on line 4 do `:4<Enter> <C-Q> }` which is, go to start of
" line 4, start a query, move to end of paragraph to grab whole of query to
" send. Ouput should appear in a second window.

nmap <expr> <C-Q> db#op_exec()
xmap <expr> <C-Q> db#op_exec()
