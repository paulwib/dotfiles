# tmux

[tmux](https://github.com/tmux/tmux/) is the terminal multiplexer allowing you to have multiple windows (tabs) and panes (splits) in one terminal session.

## Keyboard Shortcuts

* Special keys are notated as:
  - `C` - Control key, labelled as `Ctrl` on most keyboards.
  - `M` - Modifier, labelled as `Alt` and `⌥` on Mac keyboards, usually just `Alt` on other keyboards.
  - `S` - Super, labelled as `cmd` and `⌘` on Mac keyboards, varies on other keyboards but often it's the "Windows" key, labelled with some little logo.
* Keys that are pressed together are hyphenated e.g. `C-a` means press `Control` and the character `a` at the same time.
* Sequences of keys are joined together e.g. `abc` means press `a`, then `b`, then `c`.
* To avoid ambiguity keys that are pressed together followed by a sequence will be separated by a space e.g. `C-a a` means press `Control` and `a` together, then press `a`.
* Shortcuts are case sensitive, so capital letters mean press `Shift` and the key e.g. `C-a T` means press `Control` and `a` together, then `Shift` and `t` together.
* Where a value is shown in square brackets it represents potential multiple values e.g. `[n]` might mean any number.
* The default prefix for sending a tmux command is `C-b`, but I have that remapped to `C-a`, hence why all the keyboard shortcuts below start with `C-a`.
* Shortcuts that are modifications or additions to the defaults are marked with a `*` (see [tmux.conf](../home/.config/tmux/tmux.conf) for definitions).

| Keys       | Meaning                                                                             |
-------------|-------------------------------------------------------------------------------------|
| `C-a ?`    | Display the current keyboard shortcuts
| `C-a r`    | Reload the config file *
| `C-a d`    | Detach from the current session
| `C-a (`    | Switch to previous session
| `C-a )`    | Switch to next session
| `C-a L`    | Switch to previously used session
| `C-a s`    | Choose session from a list, `Enter` selects, `Esc` or `q` quits
| `C-a $`    | Rename the session (shows prompt in status line)
| `C-a c`    | Create a new window
| `C-a [n]`  | Switch to window numbered `[n]`, where `[n]` is a numeric key
| `C-a p`    | Switch to previous window
| `C-a n`    | Switch to next window
| `C-a l`    | Switch to previously used window
| `C-a w`    | Choose window from a list, `Enter` selects, `Esc` or `q` quits
| `C-a ,`    | Rename the window (shows prompt in status line)
| `C-a &`    | Kill the window (shows prompt to confirm yes/no)
| `C-a \`    | Split pane vertically *
| `C-a -`    | Split pane horizontally *
| `C-a [d]`  | Switch to pane in direction `[d]`, where `[d]` is an arrow key
| `C-a ;`    | Switch to last used pane
| `C-a z`    | Zoom the current pane to fill whole window, toggles on/off
| `C-a i`    | Type into all panes at the same time, toggles on/off *
| `C-a T`    | Rename pane *
| `C-a t`    | Show pane titles, toggles on/off *
| `C-a C-o`  | Rotate panes
| `C-a C-a`  | Cycle through panes *
| `C-a !`    | Move the current pane to a new window aka "break" pane
| `C-a M-[d]`| Resize the current pane by 5 rows/columns in direction `[d]`, where `[d]` is an arrow key
| `C-a M-1`  | Switch panes to even-horizontal layout
| `C-a M-2`  | Switch panes to even-vertical layout
| `C-a M-3`  | Switch panes to main-horizontal layout
| `C-a M-4`  | Switch panes to main-vertical layout
| `C-a M-5`  | Switch panes to tiled layout
| `C-a space`| Cycle through layouts

## Copy/Paste and the Scrollback Buffer

In tmux panes don't scroll like they might do in something like an [iTerm2][] or [tilix][]. In order to scroll back through the contents of a pane you go into copy mode. Once in copy mode you can move around and select text with either emacs or vim shortcuts depending on the setting of `mode-keys`. This value should be set automatically depending on the `$EDITOR` environment variable, but if it isn't it can be set in your config file, for example to use vim shortcuts:

```
setw -g mode-keys vi
```

| Keys       | Meaning                                                                             |
-------------|-------------------------------------------------------------------------------------|
| `C-a [`    | Go into copy mode, any vi movement keys can then be used to move around the screen
| `Space`    | Start selecting text, any vi movement keys can be used to make the selection
| `Enter`    | Copy the text into a buffer and go back to normal mode (use `q` to return to normal mode without selecting anything)
| `C-a ]`    | Paste the contents of the buffer

If you would prefer mouse scrolling and copy/pasting [it is possible](https://www.freecodecamp.org/news/tmux-in-practice-scrollback-buffer-47d5ffa71c93/).

## In terminal commands

These are commands you would type in a terminal shell. Most of them can be truncated to a couple of characters, as long as they unambiguously identify the command e.g. `a`, or `att` can be used instead of `attach`.

Note that trying to create or attach to sessions from within a tmux session will give a warning about creating nested sessions, so just avoid doing that (see links below for alternatives).

| Command                         | Meaning                                                        |
|---------------------------------|----------------------------------------------------------------|
| `tmux new-session -s work`      | Create a new session named "work"
| `tmux attach -s work`           | Attach to a session named "work"
| `tmux list-sessions`            | List all available sessions
| `tmux list-buffers`             | Show clipboard buffers
| `tmux show-buffer`              | Show clipboard content
| `tmux save-buffer -b n foo.txt` | Write contents of buffer `n` to file `foo.txt`

## In tmux commands

Typing `C-a :` in tmux will bring you to a prompt, handy for making more complex commands or rarely used things that don't need a shortcut. Type the command and press enter to apply it.

Here are some examples of useful commands:

| Command            | Meaning                                                                     |
|--------------------|-----------------------------------------------------------------------------|
| `joinp -t :1`      | Move the current pane to a new pane in window 1

## OSX

### Clipboard Issues

On OSX integration with the system clipboard has been historically quite fiddly, requiring [special wrappers](https://gist.github.com/russelldb/06873e0ad4f5ba1c4eec1b673ff4d4cd). However using a recent [iTerm2][] (v3.2.9 on Mojave at the time of writing) it seems to work out of the box, although the first time you try to copy something you will get a warning from iTerm2 that must be accepted to allow tmux to access the clipboard. It also seems to work out of the box on [Alacritty][] v0.3.2.

### Modifier key

The `M` (i.e. `Alt/Option`) key on OSX is often bound to different things on OSX that means it might not work out of the box on OSX without some tweaks.

For [iTerm2][] the [FAQ](https://www.iterm2.com/faq.html) says:

> Q: How do I make the option/alt key act like Meta or send escape codes?  
> A: Go to Preferences > Profiles tab. Select your profile on the left, and then open the Keyboard tab. At the bottom is a set of buttons that lets you select the behavior of the Option key. For most users, Esc+ will be the best choice.

I found using the left-option key didn't work, maybe because of some other OSX setting, but mapping to the right option key worked. This is quite nice as it's close to the arrow keys and my main use is for resizing panes with `C-a M-[d]`.

For [Alacritty][] it almost works out of the box, except for setting layouts with `C-a M-[n]`. Again this is likely because of some other OSX setting.

## Additional Sources and Info

* [Setting pane titles](https://stackoverflow.com/a/55845949/2455598)
* [Creating sessions from inside a session](https://stackoverflow.com/a/21272658/2455598)
* [tmux cheatsheet](https://gist.github.com/russelldb/06873e0ad4f5ba1c4eec1b673ff4d4cd)
* [Zooming tmux panes](https://sanctum.geek.nz/arabesque/zooming-tmux-panes/)
* [OSX Option key does not work as meta key in tmux](https://superuser.com/questions/649960/option-key-does-not-work-as-meta-in-tmux)
* [Change background colour of inactive pane](https://stackoverflow.com/questions/25532773/change-background-color-of-active-or-inactive-pane-in-tmux)
* [Dim inactive pane on vim](https://github.com/blueyed/vim-diminactive)
* [Vim/tmux focus events](https://github.com/tmux-plugins/vim-tmux-focus-events)

[iTerm2]:https://github.com/gnachman/iTerm2
[Alacritty]:https://github.com/jwilm/alacritty/
[Tilix]:https://github.com/gnunn1/tilix
