# osx

## Keyboard Shortcuts

* Special keys are notated as:
  - `C` - Control key, labelled as `Ctrl` on most keyboards.
  - `M` - Modifier, labelled as `Alt` and `⌥` on Mac keyboards, usually just `Alt` on other keyboards.
  - `S` - Super, labelled as `cmd` and `⌘` on Mac keyboards, varies on other keyboards but often it's the "Windows" key, labelled with some little logo.
* Keys that are pressed together are hyphenated e.g. `C-a` means press `Control` and the character `a` at the same time.
* Sequences of keys are joined together e.g. `abc` means press `a`, then `b`, then `c`.
* To avoid ambiguity keys that are pressed together followed by a sequence will be separated by a space e.g. `C-a a` means press `Control` and `a` together, then press `a`.
* Shortcuts are case sensitive, so capital letters mean press `Shift` and the key e.g. `C-a T` means press `Control` and `a` together, then `Shift` and `t` together.
* Where a value is shown in square brackets it represents potential multiple values e.g. `[n]` might mean any number.

| Keys       | Meaning                                                                             |
-------------|-------------------------------------------------------------------------------------|
| `C ↑`      | Expose - show all your workspaces and app windows
| `C ↓`      | Close expose view
| `C →`      | Move to the workspace to the left
| `C ←`      | Move to the workspace to the right
| `S-Tab`    | Switch apps
| ``S-```    | Switch open windows in an app
| `C-S-q`    | Lock screen
| `S-q`      | Quit the current app
| `S-w`      | Close the current window
| `S-Q`      | Quit all apps and log out
