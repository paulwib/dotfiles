# vim plugins

These keyboard shortcuts and sequences may be specific to my [vim](../home/.config/vim/.vimrc) and [plugin](../home/vim/plugin/settings) configuration, may not work for you.

## [NERDCommenter][]

| Command     | Effect                                                                                                                    |
| ----------- | ------------------------------------------------------------------------------------------------------------------------- |
| `<leader>/` | Toggle comments of line or selection, can be preceded by number of lines e.g. `5,/` would toggle comments on next 5 lines |

## [Conqueror of Completion][]

| Command      | Effect                                                       |
| ------------ | ------------------------------------------------------------ |
| `gd`         | Go to symbol definition                                      |
| `gy`         | Go to type definition                                        |
| `gi`         | Go to implementation                                         |
| `gr`         | Go to references, shows all files in window                  |
| `<leader>rn` | Rename all instances of a symbol across project              |
| `<leader>ca` | Open possible code actions for cursor position               |
| `<leader>sa` | Open possible code actions for source file                   |
| `<leader>fq` | Quickfix error, I think this generally goes for first option |
| `<leader>re` | Open possible refactoring actions                            |
| `:CocAction` | Open list of all possible actions with fuzzy finder          |

[Tabular]: https://github.com/godlygeek/tabular
[NERDCommenter]: https://github.com/preservim/nerdcommenter
[Conqueror of Completion]: https://github.com/neoclide/coc.nvim
