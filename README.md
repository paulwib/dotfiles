# dotfiles

> dotfiles managed with homeshick

## Install

Make sure dependencies installed:

* [bash][]
* [git][]
* [curl][]
* [myrepos][]

Then:

```
# This XDG_DATA_HOME export is only needed if not already set, should be the same as what is in home/.bash_profile
$ export XDG_DATA_HOME=${HOME}/.local
$ mr --trust-all bootstrap https://gitlab.com/paulwib/dotfiles/raw/master/home/.mrconfig
```

This is self bootstrapping, so will install `homeshick` first, then `dotfiles` and some other useful tools.

Please be responsible and check the contents of the `.mrconfig` file to understand what it is going to install.

## Conventions

Follows the [XDG spec](https://specifications.freedesktop.org/basedir-spec/basedir-spec-latest.html) for keeping config, data and caches organised and avoid cluttering the home directory with dotfiles.

Where the XDG spec is not supported directly by some pieces of software various other environment variables are set to get them to play nice. There is a [helpful guide on the Arch Wiki](https://wiki.archlinux.org/index.php/XDG_Base_Directory).

There are a few tools where it is not possible to make them conform to XDG spec and remian in the home directory:

* `.mrconfig`: [myrepos][] config file - repos must be below the directory the config file is in (unless absolute paths are used, but then the config would not be portable).
* `.bash_profile`, `.bashrc`: [bash][] config files - too difficult to fix, especially for non-interactive shells.
* `.ctags`: too many other programs depend on ths being in the conventional place, switching to [universal-ctags](https://ctags.io/) might help.
* `.tool-versions`: [asdf][] default tools - when using asdf it looks for this file to tell it what the default versions of tools it should use, looking up from the current directory until it finds a file. Having this in the home directory ensures some defaults can be set.

## Tools

Installs:

* [homeshick](https://github.com/andsens/homeshick) - to manage the dotfiles
* [fzf](https://github.com/junegunn/fzf) - fuzzy finder, used in bash and vim
* [asdf][] - tool manager, installs everything in `home/.tool-versions`

## Vim

Config requires Vim v8.1 or above. Setup is aimed at terminal Vim, not tested with a GUI Vim.

### Plugins

Plug-ins are loaded using [native package loading](https://shapeshed.com/vim-packages/) and downloaded/updated using [myrepos][]. See `home/.mrconfig` for the list of plug-ins installed.

## Themes

Uses [base16](https://github.com/chriskempson/base16) for themes. The theme name is exported in `.bash_profile` and set for these tools:

* [base16-shell](https://github.com/chriskempson/base16-shell) - terminal colours:
  - set in `home/.config/bash/apps/base16-shell`
  - won't work in Mac terminal, fine in iTerm2
* [base16-vim](https://github.com/chriskempson/base16-vim) - vim theme:
  - set in `home/.config/vim/plugin/settings/colors.vim`
  - uses vim-airline-themes to match to the current theme

## Uninstall

The included `uninstall` script will remove all git repos in `.mrconfig` (including this one). This will in effect break any symlinks to config in this repo, so the script will also clean up any broken symlinks in your home directory or in `~/.config`.

Directories will still be left behind, they can be manually deleted. It also may leave behind files that were created by tools configured by this repo, for example bash history or npm caches. It should be safe to delete anything in `~/.cache`, but be more careful with things in `~/.local`.

[bash]:https://www.gnu.org/software/bash/
[git]:https://git-scm.com/
[curl]:https://curl.haxx.se/
[myrepos]:http://myrepos.branchable.com/
[asdf]:https://github.com/asdf-vm/asdf
